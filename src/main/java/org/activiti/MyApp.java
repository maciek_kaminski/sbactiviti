package org.activiti;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.*;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.pvm.delegate.ExecutionListenerExecution;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class MyApp {

    public static void main(String[] args) {
        SpringApplication.run(MyApp.class, args);
    }

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    RepositoryService repositoryService;

    @Bean
    CommandLineRunner initialize() {
        return (args) -> {
            Map<String, Object> variables = new HashMap<>();
            variables.put("init", "dadataatata");
            ProcessInstance rootProcess = runtimeService.startProcessInstanceByKey("rootProcess", variables);
            System.out.println("Starting ROOT process with ID:" + rootProcess.getProcessInstanceId());

        };
    }

    @Bean
    JavaDelegate rootInitDelegate() {
        return (DelegateExecution dt) -> {
            System.out.println("Root Init Delegate");
            System.out.println("process with ID:" + dt.getProcessInstanceId());
        };
    }

    @Bean
    JavaDelegate rootUserTaskPreListener() {
        return (DelegateExecution execution) -> {
            System.out.println("Root User Task START Listener");
            System.out.println("process with ID:" + execution.getProcessInstanceId());
        };
    }


    @Bean
    JavaDelegate rootUserTaskListener() {
        return (DelegateExecution execution) -> {
            System.out.println("Root User Task Complete Listener");
            System.out.println("process with ID:" + execution.getProcessInstanceId());
        };
    }

    @Bean
    JavaDelegate rootEndDelegate() {
        return (DelegateExecution de) -> {
            System.out.println("Root END Delegate");
            System.out.println("process with ID:" + de.getProcessInstanceId());
        };
    }

    @Bean
    JavaDelegate subInitDelegate1() {
        return (DelegateExecution de) -> {
            System.out.println("Sub init Delegate 1");
            System.out.println("process with ID:" + de.getProcessInstanceId());
        };
    }

    @Bean
    JavaDelegate subInitDelegate2() {
        return (DelegateExecution de) -> {
            System.out.println("Sub init Delegate 2");
            System.out.println("process with ID:" + de.getProcessInstanceId());
        };
    }

    @Bean
    JavaDelegate subEndDelegate() {
        return (DelegateExecution de) -> {
            System.out.println("Sub END Delegate");
            System.out.println("process with ID:" + de.getProcessInstanceId());
        };
    }

    @Bean
    InitializingBean usersAndGroupsInitializer(final IdentityService identityService) {

        return () -> {

            Group group = identityService.newGroup("user");
            group.setName("users");
            group.setType("security-role");
            identityService.saveGroup(group);

            User admin = identityService.newUser("admin");
            admin.setPassword("admin");
            identityService.saveUser(admin);

        };
    }

}
