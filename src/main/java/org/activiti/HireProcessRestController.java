package org.activiti;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class HireProcessRestController {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ApplicantRepository applicantRepository;

    @Autowired
    TaskService taskService;

    @Autowired
    RepositoryService repositoryService;

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/start", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void startHireProcess(@RequestBody Map<String, String> data) {

        Map<String, Object> variables = new HashMap<>();
        variables.put("init", "Maciek");
        ProcessInstance rootProcess = runtimeService.startProcessInstanceByKey("rootProcess", variables);
        System.out.println("Starting ROOT process with ID:" + rootProcess.getProcessInstanceId());
    }


    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/doit", method = RequestMethod.GET)
    public void doit() {
        System.out.println("----DOIT----------------------------------------");

        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().includeProcessVariables().list();
        System.out.println("Processes: " + list.size());

        List<Execution> list2 = runtimeService.createExecutionQuery().list();
        System.out.println(list2.size());

        List<Task> list1 = taskService.createTaskQuery().includeProcessVariables().list();
        System.out.println("Tasks: "+list1.size());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/rootkill", method = RequestMethod.GET)
    public void rootkill() {
        System.out.println("----ROOTKILL----------------------------------------");
        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().includeProcessVariables().list();
        System.out.println("Processes before: " + list.size());

        List<Execution> list2 = runtimeService.createExecutionQuery().list();
        System.out.println(list2.size());

        List<Task> list1 = taskService.createTaskQuery().includeProcessVariables().list();
        System.out.println("Tasks before: "+list1.size());
        System.out.println("-------------------------------------------------");

        runtimeService.createProcessInstanceQuery().excludeSubprocesses(true).list().forEach(process->{
            runtimeService.deleteProcessInstance(process.getProcessInstanceId(),"Bo tak");
        });


        list = runtimeService.createProcessInstanceQuery().includeProcessVariables().list();
        System.out.println("Processes: " + list.size());

        list2 = runtimeService.createExecutionQuery().list();
        System.out.println(list2.size());

        list1 = taskService.createTaskQuery().includeProcessVariables().list();
        System.out.println("Tasks: "+list1.size());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/comp", method = RequestMethod.GET)
    public void completeAll() {
        System.out.println("----COMP----------------------------------------");

        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().includeProcessVariables().list();
        System.out.println("Processes before: " + list.size());

        List<Task> list1 = taskService.createTaskQuery().includeProcessVariables().list();
        System.out.println("Tasks before: "+list1.size());

        taskService.createTaskQuery().includeProcessVariables().list().forEach(task->{
            taskService.complete(task.getId());
        });


        System.out.println("-------------------------------------------------");

        List<ProcessInstance> list3 = runtimeService.createProcessInstanceQuery().includeProcessVariables().list();
        System.out.println("Processes after: " + list3.size());

        List<Execution> list2 = runtimeService.createExecutionQuery().list();
        System.out.println(list2.size());

        list1 = taskService.createTaskQuery().includeProcessVariables().list();
        System.out.println("Tasks after: "+list1.size());
    }


}